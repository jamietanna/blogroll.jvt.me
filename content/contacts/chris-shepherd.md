{
  "h": "h-card",
  "properties": {
    "name": [
      "Chris Shepherd"
    ],
    "nickname": [
      "ChrisShepherd"
    ],
    "url": [
      "https://www.thesheps.dev",
      "https://hachyderm.io/@thesheps",
      "https://sheepandram.itch.io/"
    ]
  }
}

