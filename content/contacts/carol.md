{
  "h": "h-card",
  "properties": {
    "name": [
      "Carol Gilabert"
    ],
    "nickname": [
      "Carol"
    ],
    "url": [
      "https://carol.gg",
      "https://twitter.com/CarolSaysThings",
      "https://mastodon.social/@carolgilabert"
    ]
  }
}
